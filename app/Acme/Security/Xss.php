<?php namespace Acme\Security;

use Request;

class Xss
{
    /**
     * Filters the user input.
     * @param $key
     * @return string Returns filtered user input
     */
    public static function filter($key)
    {
        return trim(strip_tags(Request::get($key)));
    }

}