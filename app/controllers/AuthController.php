<?php

use Acme\Security\Xss;

class AuthController extends BaseController {

    protected $layout = '_layouts.master';

    public function __construct()
    {
        $this->beforeFilter('guest', ['only' => 'getLogin']);
        $this->beforeFilter('auth', ['only' => 'getLogout']);
    }

	public function getLogin()
	{
        $this->layout->content = View::make('auth.login');
	}

    public function postLogin()
    {
        $email = Xss::filter('email');
        $password = Request::get('password');

        if(Auth::attempt(['email' => $email, 'password' => $password], Request::get('remember')))
        {
            if(Auth::user()->role == 'user')
                return Redirect::to('user');
            elseif(Auth::user()->role == 'admin')
                return Redirect::to('admin');
        }
        return Redirect::back()->with('message', 'Invalid E-mail and password combination.');
    }

    public function getLogout()
    {
        Auth::logout();
        return Redirect::action('AuthController@getLogin');
    }

}