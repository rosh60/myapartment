<?php
/**
 * Created by PhpStorm.
 * User: Roshan
 * Date: 20/1/14
 * Time: 9:05 PM
 */

class HomeController extends BaseController{

    protected $layout = '_layouts.master';

    public function getIndex()
    {
        $this->layout->content = View::make('home.index');
    }

}