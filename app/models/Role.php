<?php

class Role extends Eloquent
{
    protected $table = 'roles';
    protected $fillable = ['user_id', 'complaint', 'forum', 'market'];

    public function user()
    {
        return $this->belogsTo('User', 'user_id');
    }
}