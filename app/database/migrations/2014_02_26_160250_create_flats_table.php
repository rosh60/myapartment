<?php

use Illuminate\Database\Migrations\Migration;

class CreateFlatsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('flats', function($t)
        {
           $t->increments('id');
           $t->string('block', 3);
           $t->integer('door');
           $t->integer('sqft');
           $t->integer('bhk');
           $t->integer('intercomm');
           $t->integer('water_inlets');
           $t->enum('vacant', array('vacant', 'occupied'));
           $t->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('flats');
	}

}