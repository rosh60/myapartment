<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResidentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('residents', function($t){
            $t->increments('id');
            $t->string('occupation', 50);
            $t->date('dob');
            $t->date('doj');
            $t->enum('blood_group', [
                'A Positive',
                'A Negetive',
                'B Positive',
                'B Negetive',
                'O Positive',
                'O Negetive',
                'AB Positive',
                'AB Negetive'
            ]);
            $t->enum('mc', ['yes', 'no']);
            $t->string('id_proof', 100);
            $t->integer('user_id')->unsigned();
            $t->foreign('user_id')
                ->references('id')
                ->on('users');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('residents');
	}

}
