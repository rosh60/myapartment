<?php

use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create(
            'users',
            function($table){
                $table->increments('id');
                $table->string('email', 100)->unique();
                $table->string('password', 60);
                $table->string('fname', 50);
                $table->string('lname', 50);
                $table->enum('gender', ['female', 'male']);
                $table->double('mobile');
                $table->string('type', 25);
                $table->string('role', 25);
                $table->timestamps();
                $table->softDeletes();
            }
        );
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}