<?php

class UserTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
                [
                    'email'     => 'admin@gmail.com',
                    'password'  => Hash::make('admin'),
                    'role'      => 'admin'
                ],
                [
                    'email'     => 'resident@gmail.com',
                    'password'  => Hash::make('resident'),
                    'role'      => 'user'
                ]
            ]
        );
    }

}