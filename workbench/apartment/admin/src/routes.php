<?php

Route::group(['before' => 'auth|admin'], function()
{
    Route::controller('admin/residents', 'Apartment\Admin\Controllers\ResidentController');
    Route::resource('admin/flats','Apartment\Admin\Controllers\FlatsController');
    Route::controller('admin', 'Apartment\Admin\Controllers\AdminController');
});