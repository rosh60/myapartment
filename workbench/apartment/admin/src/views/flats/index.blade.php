@section('content')

@include('admin::_partials.sidebar')


<div class="container-fluid">
    <div class="row">
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

            <ul class="nav nav-tabs" id="myTab">
                <li class="active"><a href="#list" data-toggle="tab">Flats List</a></li>
                <li><a href="#new" data-toggle="tab">Add Flat</a></li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id="list">
                    <p class="help-block"></p>
                    <table class="table table-stripped">
                        <thead>
                            <tr>
                                <th>Block</th>
                                <th>Door</th>
                                <th>Square Feet</th>
                                <th>BHK</th>
                                <th>Intercomm</th>
                                <th>Water Inlets</th>
                                <th>Vacant</th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($flats as $flat)
                        <tr>
                            <td>{{ $flat->block }}</td>
                            <td>{{ $flat->door }}</td>
                            <td>{{ $flat->sqft }}</td>
                            <td>{{ $flat->bhk }}</td>
                            <td>{{ $flat->intercomm }}</td>
                            <td>{{ $flat->water_inlets }}</td>
                            <td>{{ $flat->vacant }}</td>
                            <td><a class="btn btn-primary" href="{{ route('admin.flats.show', $flat->id) }}">Show</a></td>
                            <td>{{ Form::open(['route' => ['admin.flats.destroy', $flat->id], 'method' => 'delete']) }}
                            {{ Form::submit('Delete', ['class' => 'btn btn-danger']) }}
                            {{ Form::close() }}
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="tab-pane" id="new">
                    {{ Form::open(['route' => 'admin.flats.store', 'class' => 'form-horizontal', 'id' => 'addFlat']) }}
                    <p class="help-block"></p>
                    <div class="row">
                        <div class="col-md-5">
                            <div id="ajaxinfo"></div>
                            <div class="form-group">
                                <div class="col-sm-4">
                                    <label>Flat #:</label>
                                </div>
                                <div class="col-sm-3">
                                    {{ Form::select('block', ['Block', 'A' => 'A', 'B' => 'B', 'C' => 'C', 'D' => 'D', 'E' => 'E', 'F' => 'F'], 'Block', ['class' => 'form-control'])}}
                                </div>
                                <div class="col-sm-5">
                                    {{ Form::text('door', '', ['class' => 'form-control', 'placeholder' => 'Door Number', 'maxlength' => 3])}}
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4">Square Feet</label>
                                <div class="col-sm-8">{{ Form::text('sqft', '', ['class' => 'form-control', 'placeholder' => 'Square feet'])}}</div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4">BHK</label>
                                <div class="col-sm-8">{{ Form::text('bhk', '', ['class' => 'form-control', 'placeholder' => 'BHK'])}}</div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4">Intercomm</label>
                                <div class="col-sm-8">{{ Form::text('intercomm', '', ['class' => 'form-control', 'placeholder' => 'Intercomm Number', 'maxlength' => 3])}}</div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4">Water Inlets</label>
                                <div class="col-sm-8">{{ Form::text('water_inlets', '', ['class' => 'form-control', 'placeholder' => 'Intercomm Number', 'maxlength' => 3])}}</div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4">Vacant</label>
                                <div class="col-sm-8">
                                    <label class="radio-inline">
                                        <input type="radio" name="vacant" value="vacant"> Vacant
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="vacant" value="occupied"> Occupied
                                    </label>
                                </div>
                            </div>

                            <input type="submit" class="btn btn-primary btn-lg" value="Register Flat" style="width:100%"/>

                        {{ Form::close() }}
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

@stop


@section('script')

    <script src="http://malsup.github.com/jquery.form.js"></script>

    <script>
        $(function(){
            $('#addFlat').ajaxForm({
                success: function(rsp)
                {
                    if(rsp != 'success')
                        $('#ajaxinfo').text(rsp).removeClass().addClass('alert alert-danger').fadeIn();
                    else
                    {
                        alert('Registration successful.');
                        location.reload();
                    }
                }
            });
        });
    </script>

@stop