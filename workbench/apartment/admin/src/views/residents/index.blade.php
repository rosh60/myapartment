@section('content')

@include('admin::_partials.sidebar')

<div class="container-fluid">
    <div class="row">
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

            <ul class="nav nav-tabs" id="myTab">
                <li class="active"><a href="#list" data-toggle="tab">Residents List</a></li>
                <li><a href="#new" data-toggle="tab">Add Resident</a></li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id="list">

                </div>
                <div class="tab-pane" id="new">
                    {{ Form::open(['url' => 'admin/residents', 'class' => 'form-horizontal', 'id' => 'addResident']) }}
                    <p class="help-block"></p>
                    <div class="row">
                        <div class="col-md-5">
                            <div id="ajaxResult" style="display:none;"></div>
                            <div class="form-group">
                                <div class="col-sm-4">
                                    <label>Flat #:</label>
                                </div>
                                <div class="col-sm-3">
                                    {{ Form::select('block_number', ['Block', 'A' => 'A', 'B' => 'B', 'C' => 'C', 'D' => 'D', 'E' => 'E', 'F' => 'F'], 'Block', ['class' => 'form-control'])}}
                                </div>
                                <div class="col-sm-5">
                                    {{ Form::text('door_number', '', ['class' => 'form-control', 'placeholder' => 'Door Number', 'maxlength' => 3])}}
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4">Name</label>
                                <div class="col-sm-8">{{ Form::text('name', '', ['class' => 'form-control', 'placeholder' => 'Name'])}}</div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4">Date of Birth</label>
                                <div class="col-sm-8">{{ Form::text('dob', '', ['class' => 'form-control', 'placeholder' => 'Date of Birth', 'id' => 'dob'])}}</div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4">Gender</label>
                                <div class="col-sm-8">
                                    <label class="radio-inline">
                                        <input type="radio" name="gender" value="Male"> Male
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="gender" value="Female"> Female
                                    </label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4">Blood Group</label>
                                <div class="col-sm-8">{{ Form::select('blood_group', ['Blood Group', 'A Positive' => 'A Positive', 'A Negetive' => 'A Negetive', 'B Positive' => 'B Positive', 'B Negetive' => 'B Negetive', 'AB Postive' => 'AB Postive', 'AB Negetive' => 'AB Negetive', 'O Positive' => 'O Positive', 'O Negetive' => 'O Negetive'], 'Blood Group', ['class' => 'form-control'])}}</div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4">Mobile</label>
                                <div class="col-sm-8">{{ Form::text('mobile', '', ['class' => 'form-control', 'placeholder' => 'Mobile Number', 'maxlength' => 10])}}</div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4">Intercomm</label>
                                <div class="col-sm-8">{{ Form::text('intercomm', '', ['class' => 'form-control', 'placeholder' => 'Intercomm Number', 'maxlength' => 3])}}</div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4">User Status</label>
                                <div class="col-sm-8">
                                    <label class="radio-inline">
                                        <input type="radio" name="user_status" value="Owner"> Owner
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="user_status" value="Tenant"> Tenant
                                    </label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4">Occupation</label>
                                <div class="col-sm-8">{{ Form::text('occupation', '', ['class' => 'form-control', 'placeholder' => 'Occupation'])}}</div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4">Email ID</label>
                                <div class="col-sm-8">{{ Form::text('email', '', ['class' => 'form-control', 'placeholder' => 'E-mail ID'])}}</div>
                            </div>
                            <input type="submit" class="btn btn-primary btn-lg" value="Register Resident" style="width:100%"/>

                        </div>

                        <div class="col-md-4">
                            <div id="webcam" style="">
                            </div><br/>
                            <a class="btn btn-primary" id="btn1" onclick="base64_tofield();base64_toimage()" style="width:320px">Take Snapshot</a>
                            <div style="width:200px;"><br>
                                <img id="image" />
                            </div>
                            {{ Form::hidden('photo', '', ['id' => 'formfield']) }}
                        </div>
                        {{ Form::close() }}
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

@stop

@section('script')

<script language="JavaScript" src="//ajax.googleapis.com/ajax/libs/swfobject/2.2/swfobject.js"></script>
{{ HTML::script('js/scriptcam.js') }}
<script language="JavaScript">
    $(document).ready(function() {
        $("#webcam").scriptcam({
            showMicrophoneErrors:false,
            onError:onError,
            cornerRadius:20,
            cornerColor:'e3e5e2',
            onWebcamReady:onWebcamReady,
            onPictureAsBase64:base64_tofield_and_image,
            path:'{{ URL::to("") }}/js/'
        });
    });
    function base64_tofield() {
        $('#formfield').val($.scriptcam.getFrameAsBase64());
    };
    function base64_toimage() {
        $('#image').attr("src","data:image/png;base64,"+$.scriptcam.getFrameAsBase64()).slideDown();
    };
    function base64_tofield_and_image(b64) {
        $('#formfield').val(b64);
        $('#image').attr("src","data:image/png;base64,"+b64);
    };
    function changeCamera() {
        $.scriptcam.changeCamera($('#cameraNames').val());
    }
    function onError(errorId,errorMsg) {
        $( "#btn1" ).attr( "disabled", true );
        $( "#btn2" ).attr( "disabled", true );
        alert(errorMsg);
    }
    function onWebcamReady(cameraNames,camera,microphoneNames,microphone,volume) {
        $.each(cameraNames, function(index, text) {
            $('#cameraNames').append( $('<option></option>').val(index).html(text) )
        });
        $('#cameraNames').val(camera);
    }
</script>

@stop