<?php namespace Apartment\Admin\Models;

class Flat extends \Eloquent{

    protected $guarded = ['id'];
    public static $rules = [
        'block'     => 'required|alpha_num',
        'door'      => 'required|numeric',
        'sqft'      => 'numeric',
        'bhk'       => 'numeric',
        'intercomm' => 'numeric',
        'vacant'    => 'in:vacant,occupied'
    ];

    public function user()
    {
        return $this->belogsTo('User', 'user_id');
    }

}
