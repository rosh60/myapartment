<?php namespace Apartment\Admin\Controllers;

use Request, Response, Redirect, View, Validator;
use Acme\Security\Xss;
use Apartment\Admin\Models\Flat;

class FlatsController extends \BaseController{

    protected $layout = 'admin::_layouts.master';

    public function index()
    {
        $flats = Flat::all();
        $this->layout->content = View::make('admin::flats.index', compact('flats'));
    }

    public function store()
    {
        $input = Request::all();
        $v = Validator::make($input, Flat::$rules);
        if($v->fails())
            return Response::make($v->errors()->first());

        $flat = new Flat($input);
        if($flat->save())
            return Response::make('success');
    }

    public function destroy($id)
    {
        $flat = Flat::findOrFail($id);
        $flat->delete();
        return Redirect::route('admin.flats.index');
    }

    public function show($id)
    {
        $flat = Flat::findOrFail($id);
        $this->layout->content = View::make('admin::flats.show', compact('flat'));
    }

}