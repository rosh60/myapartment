<?php namespace Apartment\Admin\Controllers;

use View;

class ResidentController extends \BaseController{

    protected $layout = 'admin::_layouts.master';

    public function getIndex()
    {
        $this->layout->content = View::make('admin::residents.index');
    }

    public function postCreate()
    {

    }

} 