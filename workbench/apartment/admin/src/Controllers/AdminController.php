<?php namespace Apartment\Admin\Controllers;

use View;

class AdminController extends \BaseController {

    protected $layout = 'admin::_layouts.master';

    public function getIndex()
    {
        $this->layout->content = View::make('admin::home');
    }

}