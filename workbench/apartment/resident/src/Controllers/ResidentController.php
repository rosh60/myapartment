<?php namespace Apartment\Resident\Controllers;

use Illuminate\Database\Console\Migrations\InstallCommand;
use View;

class ResidentController extends \BaseController{

    protected $layout = 'resident::_layouts.master';

    public function getIndex()
    {
        $this->layout->content = View::make('resident::home');
    }

}