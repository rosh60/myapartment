<!DOCTYPE html>
<html>
<head>
    <title>MyApartment</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    {{ HTML::style('css/bootstrap.css') }}
    @yield('style')
    <script src="https://code.jquery.com/jquery.js"></script>
</head>
<body style="padding-top: 70px;">
<div class="navbar navbar-fixed-top navbar-inverse" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="{{ url('/') }}">MyApartment</a>
        </div>
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav pull-right">
                @if(Auth::check())
                <li><a href="{{ url('auth/logout') }}">Logout</a></li>
                @else
                <li><a href="{{ url('auth/login') }}">Login</a></li>
                @endif
            </ul>
        </div><!-- /.nav-collapse -->
    </div><!-- /.container -->
</div><!-- /.navbar -->

@yield('content')

@yield('script')

</body>
</html>